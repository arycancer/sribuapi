module Sribuapi
  class V1::UsersController < ApplicationController
    respond_to :json
    
    def index
      @users = User.first
      respond_with(@users)
    end
    
  end
end