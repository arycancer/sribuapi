module Sribuapi
  class V1::MembersController < ApplicationController
    respond_to :json
    
    def index
      @members = Member.first
      respond_with(@members)
    end
  end
end
